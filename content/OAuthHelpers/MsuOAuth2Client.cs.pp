﻿using DotNetOpenAuth.AspNet;
using DotNetOpenAuth.AspNet.Clients;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web;

namespace $rootnamespace$.OAuthHelpers
{
    /// <summary>
    /// This class is derived from the OAuth2Client which comes standard in .NET MVC4 Internet applications.
    /// 
    /// </summary>
    public class MsuOAuth2Client : OAuth2Client
    {
        /// <summary>
        /// The authorization endpoint.
        /// </summary>
        private const string AuthorizationEndPoint = "http://oauth.dev.ais.msu.edu/oauth/authorize";

        /// <summary>
        /// The token endpoint.
        /// </summary>
        private const string TokenEndPoint = "http://oauth.dev.ais.msu.edu/oauth/token";

        /// <summary>
        /// The token endpoint.
        /// </summary>
        private const string UserInfoEndPoint = "http://oauth.dev.ais.msu.edu/oauth/me";

        /// <summary>
        /// The _app id.
        /// </summary>
        private readonly string _clientId;

        /// <summary>
        /// The _app secret.
        /// </summary>
        private readonly string _clientSecret;


        private readonly string _redirectUri;

        public MsuOAuth2Client(string clientId, string clientSecret, string redirectUri) :
            this(clientId, clientSecret, redirectUri, new Dictionary<string, object>()) { }

        public MsuOAuth2Client(string clientId, string clientSecret, string redirectUri, Dictionary<string, object> requestedInfo)
            : base("msunet")
        {
            if (string.IsNullOrWhiteSpace(clientId))
            {
                throw new ArgumentException("Client ID cannot be blank");
            }
            if (string.IsNullOrWhiteSpace(clientSecret))
            {
                throw new ArgumentException("Client Secret cannot be blank");
            }

            _clientId = clientId;
            _clientSecret = clientSecret;
            _redirectUri = redirectUri;
        }

        protected override string QueryAccessToken(Uri returnUrl, string authorizationCode)
        {
            /* Get the data ready to be POSTed */
            var postData = HttpUtility.ParseQueryString(string.Empty);
            postData.Add(new NameValueCollection
                {
                    { "grant_type", "authorization_code" },
                    { "code", authorizationCode },
                    { "client_id", _clientId },
                    { "client_secret", _clientSecret },
                    { "redirect_uri", _redirectUri },
                });

            /* Creates the Web Request*/
            var webRequest = (HttpWebRequest)WebRequest.Create(TokenEndPoint);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json";

            using (var s = webRequest.GetRequestStream())
            using (var sw = new StreamWriter(s))
                sw.Write(postData.ToString());
            using (var webResponse = webRequest.GetResponse())
            {
                var responseStream = webResponse.GetResponseStream();
                if (responseStream == null)
                    return null;

                using (var reader = new StreamReader(responseStream))
                {
                    var response = reader.ReadToEnd();
                    var json = JObject.Parse(response);
                    var accessToken = json.Value<string>("access_token");
                    return accessToken;
                }
            }
        }

        public override AuthenticationResult VerifyAuthentication(HttpContextBase context, Uri returnPageUrl)
        {
            var code = context.Request.QueryString["code"];
            if (string.IsNullOrEmpty(code))
            {
                return AuthenticationResult.Failed;
            }
            
            string accessToken = this.QueryAccessToken(returnPageUrl, code);
            if (accessToken == null)
            {
                return AuthenticationResult.Failed;
            }
            IDictionary<string, string> userData = this.GetUserData(accessToken);
            if (userData == null)
            {
                return AuthenticationResult.Failed;
            }
            string id = userData["uid"];
            string name = userData["email"];
            userData["accessToken"] = accessToken;
            return new AuthenticationResult(isSuccessful:true, provider:this.ProviderName, providerUserId:id, userName: name, extraData:userData);
        }

        protected override IDictionary<string, string> GetUserData(string accessToken)
        {
            var uri = BuildUri(UserInfoEndPoint, new NameValueCollection { { "access_token", accessToken } });

            var webRequest = (HttpWebRequest)WebRequest.Create(uri);

            using (var webResponse = webRequest.GetResponse())
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null)
                    return null;

                using (var textReader = new StreamReader(stream))
                {
                    var json = textReader.ReadToEnd();
                    var msuInfo = JsonConvert.DeserializeObject<MSUOAuth2ResponseSchema>(json);
                    var extraData = msuInfo.info;
                    extraData.Add("uid", msuInfo.uid);
                    return extraData;
                }
            }
        }

        protected override Uri GetServiceLoginUrl(Uri returnUrl)
        {
            return BuildUri(AuthorizationEndPoint, new NameValueCollection
            {
                {"response_type", "code"},
                {"client_id", _clientId},
                {"redirect_uri", _redirectUri}
            });
        }

        private static Uri BuildUri(string baseUri, NameValueCollection queryParameters)
        {
            var q = System.Web.HttpUtility.ParseQueryString(string.Empty);
            q.Add(queryParameters);
            var builder = new UriBuilder(baseUri) { Query = q.ToString() };
            return builder.Uri;
        }

        /// <summary>
        /// This is here in case state ever gets passed back from MSU.
        /// This should be called before verifying the request, so that the url is rewritten to support this.
        /// </summary>
        public static void RewriteRequest()
        {
            var ctx = HttpContext.Current;

            var stateString = HttpUtility.UrlDecode(ctx.Request.QueryString["state"]);
            if (stateString == null || !stateString.Contains("__provider__=msunet"))
                return;

            var q = HttpUtility.ParseQueryString(stateString);
            q.Add(ctx.Request.QueryString);
            q.Remove("state");

            ctx.RewritePath(ctx.Request.Path + "?" + q);
        }
    }

    public class MSUOAuth2ResponseSchema
    {
        public string provider { get; set; }
        public string uid { get; set; }
        public Dictionary<string, string> info { get; set; }
    }
}