using System;
using System.Collections.Generic;
using Microsoft.Web.WebPages.OAuth;
using $rootnamespace$.Models;
using $rootnamespace$.OAuthHelpers;
using System.Configuration;

namespace $rootnamespace$
{
	public class MsuAuthConfig
	{
		public static void RegisterAuth()
        {
            var msuExtra = new Dictionary<string, object>();
            msuExtra.Add("Icon", "~/Content/msunet.png");
            var msuClient = new MsuOAuth2Client(ConfigurationManager.AppSettings["MsuClientId"],
                ConfigurationManager.AppSettings["MsuClientSecret"],
                ConfigurationManager.AppSettings["MsuRedirectUri"], msuExtra);

            OAuthWebSecurity.RegisterClient(msuClient, "MSU", msuExtra);
        }
	}
}