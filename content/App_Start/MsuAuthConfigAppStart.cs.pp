using System;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof($rootnamespace$.App_Start.MsuAuthConfigAppStart), "Start")]

namespace $rootnamespace$.App_Start {
    public static class MsuAuthConfigAppStart {
        public static void Start() {
            MsuAuthConfig.RegisterAuth();
        }
    }
}